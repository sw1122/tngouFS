package net.tngou.tnfs.dao;



import net.tngou.jtdb.Field;
import net.tngou.jtdb.SortField;
import net.tngou.tnfs.util.PageUtil;


public interface DaoImp<T> {

	
	public void	save( T pojo);
	
	public PageUtil query(int page,int size,SortField sortField, Field ...fields) ;


	
	
}
