/*
Copyright 2014年10月9日 tngou

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


package net.tngou.tnfs.util;   

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;



import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**   
 * Filename:    HttpConfig.java   
 * Copyright:   Copyright (c)2014 
 * Company:     rimi  
 * @version:    1.0   
 * @since:      JDK 1.7.0_21  
 * Create at:   2014年10月9日 上午10:16:32   
 * Description:  
 *   
 * Modification History:   
 * Date             Author      Version     Description   
 * ----------------------------------------------------------------- 
 * 2014年10月9日           陈磊                  1.0         1.0 Version   
 */

public class HttpConfig {
	private static final Logger log= LoggerFactory.getLogger(HttpConfig.class);
	private String UserAgent;
	private int		timeout;
	private String	AcceptEncoding;
	private String	AcceptLanguage;
	private int	MaxTotal;
	private int	MaxPerRoute;
	private String tnfspath;    //存储地址
	private String tnfsurl;     // 天狗文件服务器
	private String imgurl;
	private String fileurl;
	private String videourl;
	
	private int minwidth; //图片最小宽度
	private int minheight;//图片最小高度
	private int maxwidth; //图片最大宽度
	private int maxheight;//图片最大高度
	/**
	 * 单例方法
	 * @return
	 */
	public final static HttpConfig getInstance (){
		return new HttpConfig();
	}
	private HttpConfig()
	{
		try {
		  File propertiesFile = new File("http.properties");
		  Configurations configs = new Configurations();
		   PropertiesConfiguration config = configs.properties(propertiesFile);
		
			Map<String,  Object> cp_props = new HashMap<String, Object>();
			
			Iterator<String> iterable = config.getKeys();
			while (iterable.hasNext()) 
			{
				String skey=iterable.next();
				cp_props.put(skey, config.getProperty(skey));
			}
		
			
			BeanUtils.populate(this, cp_props);
		} catch (ConfigurationException | IllegalAccessException | InvocationTargetException e) {
			
			log.error("解析配置文件http.properties错误");
//			e.printStackTrace();
		}
	}
	
	public String getUserAgent() {
		return UserAgent;
	}
	public void setUserAgent(String userAgent) {
		UserAgent = userAgent;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public String getAcceptEncoding() {
		return AcceptEncoding;
	}
	public void setAcceptEncoding(String acceptEncoding) {
		AcceptEncoding = acceptEncoding;
	}
	public String getAcceptLanguage() {
		return AcceptLanguage;
	}
	public void setAcceptLanguage(String acceptLanguage) {
		AcceptLanguage = acceptLanguage;
	}
	public int getMaxTotal() {
		return MaxTotal;
	}
	public void setMaxTotal(int maxTotal) {
		MaxTotal = maxTotal;
	}
	public int getMaxPerRoute() {
		return MaxPerRoute;
	}
	public void setMaxPerRoute(int maxPerRoute) {
		MaxPerRoute = maxPerRoute;
	}
	
	
	
	public String getTnfsurl() {
		return tnfsurl;
	}
	public void setTnfsurl(String tnfsurl) {
		this.tnfsurl = tnfsurl;
	}
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	public String getFileurl() {
		return fileurl;
	}
	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}
	public String getVideourl() {
		return videourl;
	}
	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	public String getTnfspath() {
		return tnfspath;
	}
	public void setTnfspath(String tnfspath) {
		this.tnfspath = tnfspath;
	}
	public int getMinwidth() {
		return minwidth;
	}
	public void setMinwidth(int minwidth) {
		this.minwidth = minwidth;
	}
	public int getMinheight() {
		return minheight;
	}
	public void setMinheight(int minheight) {
		this.minheight = minheight;
	}
	public int getMaxwidth() {
		return maxwidth;
	}
	public void setMaxwidth(int maxwidth) {
		this.maxwidth = maxwidth;
	}
	public int getMaxheight() {
		return maxheight;
	}
	public void setMaxheight(int maxheight) {
		this.maxheight = maxheight;
	}
	
	
	
}
  

