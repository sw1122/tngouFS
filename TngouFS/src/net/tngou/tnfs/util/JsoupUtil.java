package net.tngou.tnfs.util;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class JsoupUtil {

	
	public static String clean(String html) {
		
		
		Whitelist whitelist = new Whitelist()
        .addTags(
                 "b", "blockquote", "br", "cite", "code", "dd", "dl", "dt", "em",
                "i", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong", "sub",
                "sup", "u", "ul");

		 html=Jsoup.clean( html,  whitelist);
		 
		return html;
		
	}
}
